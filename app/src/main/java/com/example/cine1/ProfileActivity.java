package com.example.cine1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


public class ProfileActivity extends AppCompatActivity {

    private TextView txt_nombre, txt_apellido, txt_email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);

        txt_nombre = (TextView)findViewById(R.id.txt_nombre_perfil);
        txt_apellido = (TextView)findViewById(R.id.txt_apellido_perfil);
        txt_email = (TextView)findViewById(R.id.txt_email_perfil);

    }
}
