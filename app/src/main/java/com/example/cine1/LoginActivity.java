package com.example.cine1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.cine1.Api.APIService;
import com.example.cine1.Api.ApiUtils;
import com.example.cine1.Model.LoginResponse;


import java.io.IOException;
import java.io.OutputStreamWriter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private APIService mAPIService;
    private Button btn_registrar;
    private Button btn_iniciarsession;
    private Intent nextActivity;
    private EditText txt_user_user;
    private EditText txt_user_pass;
    private SharedPreferences preferences;
    private String key_name = "login";
    private String token = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        boolean login = preferences.getBoolean(key_name, false);
        if (login) {
            nextActivity = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(nextActivity);
            Animatoo.animateCard(this);
            finish();
        }
        mAPIService = ApiUtils.getAPIService();
        txt_user_user = (EditText) findViewById(R.id.txt_user_user);
        txt_user_pass = (EditText) findViewById(R.id.txt_user_pass);
        btn_registrar = (Button) findViewById(R.id.btn_registrar_user);
        btn_iniciarsession = (Button) findViewById(R.id.btn_iniciarsession);

//        managerUsuario = new ManagerUsuario();

//
//        btn_registrar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                nextActivity = new Intent(LoginActivity.this, RegisterActivity.class);
//                startActivityForResult(nextActivity, 1);
//            }
//        });


        btn_iniciarsession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();

            }
        });


    }
//se usaba para facebook
//    @Override
//    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == 777) {
//
//            objusuario = (Usuario) data.getParcelableExtra("usuario");
//            if (objusuario != null) {
//                Log.e("Aceept", "Usuario no es nulo");
//                managerUsuario.add(objusuario);
//
//                SuccessLogin(token);
//
//                nextActivity = new Intent(LoginActivity.this, MainActivity.class);
//                nextActivity.putExtra("usuario", objusuario);
//                startActivity(nextActivity);
//            }
//        }
//
//
//    }


    private void SuccessLogin(String token) {
        //Usuario Inicio Session Correctamente
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key_name, true);
        editor.putString("token", token);
        editor.commit();
        AniadirALog(true);
    }


    private void userLogin() {

        String email = txt_user_user.getText().toString().trim();
        String password = txt_user_pass.getText().toString().trim();

        if (email.isEmpty()) {
            txt_user_user.setError("Email is required");
            txt_user_user.requestFocus();
            return;
        }

    /*    if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError("Enter a valid email");
            editTextEmail.requestFocus();
            return;
        }*/

        if (password.isEmpty()) {
            txt_user_pass.setError("Password required");
            txt_user_pass.requestFocus();
            return;
        }
        sendPost(email, password);
    }


    public void sendPost(String user, String password) {

        mAPIService.userLogin(user, password).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {

                    LoginResponse loginResponse = response.body();

//                    System.out.println(loginResponse.toString());
                    if (loginResponse.getUser() != null) {
                        SuccessLogin(loginResponse.getToken());
                        Toast.makeText(LoginActivity.this, "Hola " + loginResponse.getUser().getUsername(), Toast.LENGTH_SHORT).show();

                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        i.putExtra("usuario",response.body().getUser());
                        startActivity(i);
                    } else if (loginResponse.getUser() == null) {
                        Toast.makeText(LoginActivity.this, loginResponse.getMensaje(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(LoginActivity.this, "Ocurrio un error :     " + loginResponse.getMensaje(), Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                System.out.println("LoginActivity.onFailure");
                t.printStackTrace();
                System.out.println("Unable to submit post to API");
            }
        });
    }

    private void AniadirALog(boolean exito) {

        try {
            OutputStreamWriter osw = new OutputStreamWriter(openFileOutput("Log.txt", Activity.MODE_APPEND));
            osw.write("Operacion Login " + (exito ? "exito" : "fracaso") + "\n");
            osw.flush();
            osw.close();

        } catch (IOException e) {
            Log.e("Error", "Error " + e.toString());
        }

    }


}
