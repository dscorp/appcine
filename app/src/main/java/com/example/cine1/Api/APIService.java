package com.example.cine1.Api;

import com.example.cine1.Model.LoginResponse;
import com.example.cine1.Model.PeliculasResponse;



import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface APIService {


//    @FormUrlEncoded
//    @POST("createuser")
//    Call<DefaultResponse> createUser(
//            @Field("email") String email,
//            @Field("password") String password,
//            @Field("name") String name,
//            @Field("school") String school
//    );

    @FormUrlEncoded
    @POST("usuario/login")
    Call<LoginResponse> userLogin(
            @Field("username") String username,
            @Field("password") String password
    );

    @GET("pelicula")
    Call<PeliculasResponse>  ListarPeliculas();


    @GET("pelicula/estrenos")
    Call<PeliculasResponse> ListarPeliculasEstreno();

//    @GET("allusers")
//    Call<UsersResponse> getUsers();
//
//    @FormUrlEncoded
//    @PUT("updateuser/{id}")
//    Call<LoginResponse> updateUser(
//            @Path("id") int id,
//            @Field("email") String email,
//            @Field("name") String name,
//            @Field("school") String school
//    );
//
//    @FormUrlEncoded
//    @PUT("updatepassword")
//    Call<DefaultResponse> updatePassword(
//            @Field("currentpassword") String currentpassword,
//            @Field("newpassword") String newpassword,
//            @Field("email") String email
//    );
//
//    @DELETE("deleteuser/{id}")
//    Call<DefaultResponse> deleteUser(@Path("id") int id);

}
