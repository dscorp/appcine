package com.example.cine1.Api;

public class ApiUtils {

    private ApiUtils() {}

    public static final String URL_SERVER = "http://192.168.137.84:8000/";


    public static final String BASE_URL = URL_SERVER+"api/";

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
