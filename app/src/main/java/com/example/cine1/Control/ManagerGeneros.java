package com.example.cine1.Control;

import com.example.cine1.Enum.Genero;

import java.util.ArrayList;
import java.util.List;

public class ManagerGeneros {
    private List<Genero> generos;

    public ManagerGeneros(){
        generos = new ArrayList<>();
    }

    public void add(Genero g){
        generos.add(g);
    }

    public String toString(){
        String text_generos = "";
        for (int i=0; i< generos.size();i++){
            text_generos += generos.get(i).getValue() + ", " ;
        }
        return text_generos;
    }
}
