package com.example.cine1.Control;

import com.example.cine1.Model.Pelicula;

import java.io.Serializable;
import java.util.ArrayList;

public class ManagerPelicula implements Serializable {
    private ArrayList<Pelicula> peliculas;
    private ArrayList<Pelicula> peliculas_lista;
    private int id_pelicula_actual;
    private Pelicula peliculaactual;

    public ManagerPelicula(ArrayList<Pelicula> peliculas) {
        this.peliculas = peliculas;
        peliculas_lista = new ArrayList<>();

        if(peliculas.size()>0) {
            this.setPeliculaAcutal(peliculas.get(0).getId());
        }
        }

    public Pelicula getPeliculaActual() {
        return peliculaactual;
    }


    public void setPeliculaAcutal(int idpelicula) {
        this.id_pelicula_actual = idpelicula;
        ProcesarCambio();
    }

    private void ProcesarCambio() {
        //Definir pelicula actual por ID Pelicula

        Pelicula auxpeliculaactual;
        for (int a = 0; a < peliculas.size(); a++) {
            if (peliculas.get(a).getId() == id_pelicula_actual) {
                peliculaactual = peliculas.get(a);
            }
        }

        peliculas_lista.clear();
        peliculas_lista = (ArrayList<Pelicula>) peliculas.clone();
        //Remover la pelicula actual
        peliculas_lista.remove(peliculaactual);
    }

    public ArrayList<Pelicula> get_allpeliculas() {
        return peliculas;
    }

    public ArrayList<Pelicula> getPeliculas_lista() {
        return peliculas_lista;
    }

    public ArrayList<Pelicula> getPeliculas() {
        return peliculas;
    }

    public void setPeliculas(ArrayList<Pelicula> peliculas) {
        this.peliculas = peliculas;
    }


//    private void Cargardatos()
//    {
//        //FECHA
//
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
//        Date  fecha = new Date();
//        simpleDateFormat.format(fecha);
//
//        //PAIS
//        Pais pais = new Pais();
//        pais.setNombre("EE.UU");
//
//        //PELICULA 01
//        Pelicula peli01 = new Pelicula();
//        peli01.setIdPelicula(1);
//        peli01.setUrlImagen(R.drawable.peli01);
//        peli01.setUrlImagenLarge(R.drawable.peli01_large);
//        peli01.setTitulo("MI MASCOTA ES UN LEÓN");
//        peli01.setDescripcion("Cuando sus padres deciden mudarse a África, Mia siente que su vida se convertirá en una pesadilla. Todo hasta que conoce a Charlie, un león blanco salvaje con el que desarrolla una conexión única. Juntos demuestran que no hay amistades imposibles y que lo increíble puede hacerse realidad. Una historia en la que la verdadera aventura será entregar tu corazón.");
//        peli01.setFechaEstreno(fecha);
//        peli01.setEsEstreno(true);
//        peli01.setDuracion(98);
//        peli01.getManagerGeneros().add(Genero.AVENTURA);
//        peli01.getManagerGeneros().add(Genero.FAMILIAR);
//        peli01.getManagerActors().add("Mélanie Laurent");
//        peli01.getManagerActors().add("Langley Kirkwood");
//        peli01.getManagerActors().add("Daniah De Villiers");
//        peli01.getManagerActors().add("Brandon Auret");
//        peli01.getManagerActors().add("Ryan McLennan");
//        peli01.getManagerActors().add("Lionel Newton");
//        peli01.setPais(pais);
//        this.peliculas.add(peli01);
//
//        //PELICULA 02
//        Pelicula peli02 = new Pelicula();
//        peli02.setIdPelicula(2);
//        peli02.setUrlImagen(R.drawable.peli02);
//        peli02.setUrlImagenLarge(R.drawable.peli02_large);
//        peli02.setTitulo("X-MEN: DARK PHOENIX");
//        peli02.setDescripcion("los X-MEN se enfrentan a su enemigo más temible y poderoso: uno de los suyos, Jean Grey. Durante una misión de rescate en el espacio, Jean casi muere cuando es golpeada por una fuerza cósmica misteriosa. A su regreso a casa, esta fuerza no sólo la hace infinitamente más poderosa, sino también mucho más inestable. Jean, quien lucha en su interior contra esta entidad, desencadena sus poderes en formas que no puede comprender ni dominar. Al estar en un espiral fuera de control, y lastimar a aquellos que más ama, Jean comienza a deshacer la mismísima estructura que mantiene unidos a los X-Men. Ahora, con esta familia cayéndose a pedazos, deberán encontrar una manera de unirse no sólo para salvar el alma de Jean, sino también para salvar a nuestro planeta de extraterrestres que desean convertir esta fuerza en un arma y gobernar la galaxia.");
//        peli02.setFechaEstreno(fecha);
//        peli02.setEsEstreno(true);
//        peli02.setDuracion(118);
//        peli02.getManagerGeneros().add(Genero.AVENTURA);
//        peli02.getManagerGeneros().add(Genero.ACCION);
//        peli02.getManagerGeneros().add(Genero.CIENCIAFICCION);
//        peli02.getManagerGeneros().add(Genero.SUPERHEROES);
//        peli02.getManagerActors().add("Sophie Turner");
//        peli02.getManagerActors().add("Alexandra Shipp");
//        peli02.getManagerActors().add("Evan Jonigkeit");
//        peli02.getManagerActors().add("Jennifer Shrader");
//        peli02.getManagerActors().add("Jessica Michelle");
//        peli02.getManagerActors().add("James Andrew");
//        peli02.setPais(pais);
//        this.peliculas.add(peli02);
//
//        //PELICULA 03
//        Pelicula peli03 = new Pelicula();
//        peli03.setIdPelicula(3);
//        peli03.setUrlImagen(R.drawable.peli03);
//        peli03.setUrlImagenLarge(R.drawable.peli03_large);
//        peli03.setTitulo("EL SOL TAMBIÉN ES UNA ESTRELLA");
//        peli03.setDescripcion("¿Y si te dijera que podría hacer que te enamores de mí en un día?\" El romántico Daniel Bae y la pragmática Natasha Kingsley se conocen de una manera peculiar. Las chispas vuelan de inmediato entre estos dos extraños, que tal vez nunca se hubieran encontrado si el destino no les hubiera dado un pequeño empujón. Pero, ¿será el destino suficiente para estos enamorados de las estrellas a la suerte? Ahora deberán ir contra el reloj, pues Natasha está luchando contra la deportación de su familia tan ferozmente como lo hace contra sus sentimientos por Daniel, quien solo quiere convencerla de que su destino es estar juntos. ¿Lo lograrán?.");
//        peli03.setFechaEstreno(fecha);
//        peli03.setEsEstreno(true);
//        peli03.setDuracion(100);
//        peli03.getManagerGeneros().add(Genero.DRAMA);
//        peli03.getManagerGeneros().add(Genero.ROMANCE);
//        peli03.getManagerActors().add("Miriam A. Hyman");
//        peli03.getManagerActors().add("Assibey Blake");
//        peli03.getManagerActors().add("Jordan Williams");
//        peli03.getManagerActors().add("Anais Lee");
//        peli03.getManagerActors().add("Camrus Johnson");
//        peli03.getManagerActors().add("Keong Sim");
//        peli03.setPais(pais);
//        this.peliculas.add(peli03);
//
//
//        //PELICULA 04
//        Pelicula peli04 = new Pelicula();
//        peli04.setIdPelicula(4);
//        peli04.setUrlImagen(R.drawable.peli04);
//        peli04.setUrlImagenLarge(R.drawable.peli04_large);
//        peli04.setTitulo("MA");
//        peli04.setDescripcion("Una mujer solitaria (Octavia Spencer) entabla amistad con un grupo de adolescentes y decide invitarles a una fiesta en su casa. Justo cuando el grupo de jóvenes piensa que su suerte no puede ir a mejor, una serie de extraños acontecimientos comienzan a suceder, poniendo en tela de juicio las intenciones de su nueva y misteriosa amiga.");
//        peli04.setFechaEstreno(fecha);
//        peli04.setEsEstreno(true);
//        peli04.setDuracion(99);
//        peli04.getManagerGeneros().add(Genero.TERROR);
//        peli04.getManagerGeneros().add(Genero.TRILLER);
//        peli04.getManagerActors().add("Octavia Spencer");
//        peli04.getManagerActors().add("Allison Janney");
//        peli04.getManagerActors().add("Juliette Lewis");
//        peli04.getManagerActors().add("Diana Silvers");
//        peli04.getManagerActors().add("Luke Evans");
//        peli04.getManagerActors().add("McKaley Miller");
//        peli04.setPais(pais);
//        this.peliculas.add(peli04);
//
//        //PELICULA 05
//        Pelicula peli05 = new Pelicula();
//        peli05.setIdPelicula(5);
//        peli05.setUrlImagen(R.drawable.peli05);
//        peli05.setUrlImagenLarge(R.drawable.peli05_large);
//        peli05.setTitulo("CREED II");
//        peli05.setDescripcion("Adonis Creed se debate entre las obligaciones personales y el entrenamiento para su próxima gran pelea, con el desafío de su vida por delante. Enfrentarse a un oponente que tiene vínculos con el pasado de su familia solo intensifica su inminente batalla en el ring. Afortunadamente Rocky Balboa está a su lado a lo largo de todo el camino, y juntos se cuestionarán por lo que vale la pena luchar y descubrirán que nada es más importante que la familia... Secuela de \"Creed\", el spin-off de Rocky que en 2015 obtuvo muy buenas críticas y dio inicio a una nueva saga.");
//        peli05.setFechaEstreno(fecha);
//        peli05.setEsEstreno(true);
//        peli05.setDuracion(130);
//        peli05.getManagerGeneros().add(Genero.DRAMA);
//        peli05.getManagerGeneros().add(Genero.DEPORTE);
//        peli05.getManagerActors().add("Michael B. Jordan");
//        peli05.getManagerActors().add("Sylvester Stallone");
//        peli05.getManagerActors().add("Tessa Thompson ");
//        peli05.setPais(pais);
//        this.peliculas.add(peli05);
//
//        //PELICULA 06
//        Pelicula peli06 = new Pelicula();
//        peli06.setIdPelicula(6);
//        peli06.setUrlImagen(R.drawable.peli06);
//        peli06.setUrlImagenLarge(R.drawable.peli06_large);
//        peli06.setTitulo("SIEMPRE A TU LADO");
//        peli06.setDescripcion("Charlie vive en un pequeño pueblo de la costa del Pacífico. Aunque es un gran marinero, le conceden una beca que le alejará de ese lugar sin futuro. Sin embargo, su sueño se desvanece cuando su hermano menor muere en un accidente de tráfico.");
//        peli06.setEsEstreno(true);
//        peli06.setFechaEstreno(fecha);
//        peli06.setDuracion(99);
//        peli06.getManagerGeneros().add(Genero.DRAMA);
//        peli06.getManagerGeneros().add(Genero.FANTASTICO);
//        peli06.getManagerGeneros().add(Genero.ROMANCE);
//        peli06.getManagerActors().add("Zac Efron");
//        peli06.getManagerActors().add("Amanda Crew");
//        peli06.getManagerActors().add("Charlie Tahan");
//        peli06.setPais(pais);
//        this.peliculas.add(peli06);
//    }

}
