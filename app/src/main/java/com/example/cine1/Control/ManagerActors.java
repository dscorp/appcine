package com.example.cine1.Control;

import java.util.ArrayList;
import java.util.List;

public class ManagerActors {
    private List<String> actores;

    public ManagerActors(){
        actores = new ArrayList<>();
    }

    public  void add(String a){
        actores.add(a);
    }

    public String toString(){
        String text_actores= "";
        for (int i=0;i< actores.size();i++){
            text_actores += actores.get(i)+ ", ";
        }
        return text_actores;
    }
}
