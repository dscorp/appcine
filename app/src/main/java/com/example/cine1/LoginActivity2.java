//package com.example.cine1;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import com.example.cine1.Control.ManagerUsuario;
//import com.example.cine1.Model.Usuario;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.GraphRequest;
//import com.facebook.GraphResponse;
//import com.facebook.login.LoginResult;
//import com.facebook.login.widget.LoginButton;
//
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//
//public class LoginActivity2 extends AppCompatActivity {
//
//    private Button btn_registrar;
//    private Button btn_iniciarsession;
//    private Intent nextActivity;
//    private ManagerUsuario managerUsuario;
//    private EditText txt_user_user;
//    private EditText txt_user_pass;
//    private Usuario objusuario;
//    private CallbackManager callbackManager;
//    private SharedPreferences preferences;
//    private String key_name = "login";
//    private String Archivo = "Log.txt";
////    DbHelper dbHelper;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);
////        dbHelper = new DbHelper(this);
////        dbHelper.GetCine();
//        preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
//        boolean login =  preferences.getBoolean(key_name,false);
//        if (login){
//            nextActivity = new Intent(LoginActivity2.this,MainActivity.class);
//            startActivity(nextActivity);
//            finish();
//        }
//
//        callbackManager = CallbackManager.Factory.create();
//        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setReadPermissions("email");
//
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                getUserDetails(loginResult);
//            }
//
//            @Override
//            public void onCancel() {
//                Toast.makeText(getApplicationContext(),"Login Cancelado",Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onError(FacebookException error) {
//                Toast.makeText(getApplicationContext(),"Login Error",Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        //FacebookSdk.sdkInitialize(getApplicationContext());
//        //AppEventsLogger.activateApp(this);
//
//        txt_user_user = (EditText)findViewById(R.id.txt_user_user);
//        txt_user_pass = (EditText)findViewById(R.id.txt_user_pass);
//
//
//        btn_registrar = (Button)findViewById(R.id.btn_registrar_user);
//        btn_iniciarsession = (Button)findViewById(R.id.btn_iniciarsession);
//
//        managerUsuario = new ManagerUsuario();
//
//        btn_iniciarsession.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(txt_user_user.getText().equals("") || txt_user_pass.getText().equals("")){
//                    Toast.makeText(getApplicationContext(),"Click en la pelicula",Toast.LENGTH_SHORT).show();
//                    return;
//                }
//
//                objusuario = managerUsuario.login(txt_user_user.getText().toString(),txt_user_pass.getText().toString());
//                if(objusuario == null){
//                    Toast.makeText(getApplicationContext(),"Combinacion Incorrecta",Toast.LENGTH_SHORT).show();
//                    Toast.makeText(getApplicationContext(),txt_user_user.getText().toString() + " "+ txt_user_pass.getText().toString()   ,Toast.LENGTH_SHORT).show();
//                    return;
//                }
//
//                SuccessLogin();
//
//                nextActivity = new Intent(LoginActivity2.this,MainActivity.class);
//                nextActivity.putExtra("usuario",objusuario);
//                startActivity(nextActivity);
//            }
//        });
//
//        btn_registrar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                nextActivity = new Intent(LoginActivity2.this,RegisterActivity.class);
//                startActivityForResult(nextActivity,1);
//            }
//        });
//
//    }
//
//    @Override
//    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (resultCode == 777) {
//
//            objusuario = (Usuario) data.getParcelableExtra("usuario");
//            if(objusuario != null){
//                Log.e("Aceept","Usuario no es nulo");
//                managerUsuario.add(objusuario);
//
//                SuccessLogin();
//
//                nextActivity = new Intent(LoginActivity2.this,MainActivity.class);
//                nextActivity.putExtra("usuario",objusuario);
//                startActivity(nextActivity);
//            }
//        }
//
//        callbackManager.onActivityResult(requestCode, resultCode, data);
//    }
//
//    protected void getUserDetails(LoginResult loginResult) {
//        GraphRequest data_request = GraphRequest.newMeRequest(
//                loginResult.getAccessToken(),
//                new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject json_object, GraphResponse response) {
//                        try {
//                            objusuario = new Usuario();
//                            objusuario.setIdUsuario(7);
//                            objusuario.setUsuario(json_object.getString("name"));
//                            objusuario.setPassword(json_object.getString("id"));
//                            objusuario.setNombre(json_object.getString("name"));
//                            objusuario.setApellido("");
//                            objusuario.setEmail(json_object.getString("email"));
//                        }catch (Exception e){
//                            Toast.makeText(getApplicationContext(),"Error Parseando datos",Toast.LENGTH_SHORT).show();
//                        }
//                        nextActivity = new Intent(LoginActivity2.this, MainActivity.class);
//                        nextActivity.putExtra("usuario", objusuario);
//                        startActivity(nextActivity);
//                    }
//
//                });
//        Bundle permission_param = new Bundle();
//        permission_param.putString("fields", "id,name,email, picture.width(120).height(120)");
//        data_request.setParameters(permission_param);
//        data_request.executeAsync();
//    }
//
//    protected void onResume() {
//        super.onResume();
//        // Logs 'install' and 'app activate' App Events.
//        //AppEventsLogger.activateApp(this);
//    }
//
//    //@Override
//    protected void onPause() {
//        super.onPause();
//        // Logs 'app deactivate' App Event.
//        //AppEventsLogger.deactivateApp(this);
//    }
//
//    private void SuccessLogin(){
//        //Usuario Inicio Session Correctamente
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.putBoolean(key_name,true);
//        editor.commit();
//        AniadirALog(true);
//    }
//
//    private void AniadirALog(boolean exito){
//        try {
//            OutputStreamWriter osw = new OutputStreamWriter(openFileOutput(Archivo, Activity.MODE_APPEND ));
//            osw.write("Operacion Login " + (exito ? "exito" : "fracaso") + "\n" );
//            osw.flush();
//            osw.close();
//        }catch (IOException e){
//            Log.e("Error","Error " + e.toString());
//        }
//    }
//}
