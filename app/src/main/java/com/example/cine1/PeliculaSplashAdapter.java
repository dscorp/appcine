package com.example.cine1;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cine1.Model.Pelicula;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

public class PeliculaSplashAdapter extends RecyclerView.Adapter<PeliculaSplashAdapter.ViewHolder> {

    private List<Pelicula> peliculas;
    private Context context;
    private TextView txt_nombrepelicula,txt_generospelicula,txt_fechaestreno,txt_pais_pelicula,txt_generospelicula_,txt_reparto;
    private MainActivity mainActivity;

    public PeliculaSplashAdapter(List<Pelicula> peliculas,Context context) {

        this.peliculas = peliculas;
        this.context=context;
        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxx"+peliculas.size());
        mainActivity = (MainActivity) context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout linearLayout;
        private ImageView imageView;
        private TextView textView;

        public ViewHolder(@NonNull View v) {
            super(v);

            imageView = (ImageView) itemView.findViewById(R.id.ivSplashPeliculasList);
            textView = (TextView) itemView.findViewById(R.id.tvSplashPeliculasList);
            linearLayout=(LinearLayout) itemView.findViewById(R.id.llcvsplash);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.itemscvpeliculassplash,viewGroup,false);
        ViewHolder vh = new ViewHolder(v);
        context=viewGroup.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int i) {
        holder.textView.setText(peliculas.get(i).getTitulo());
//        holder.imageView.setImageResource(peliculas.get(i).getUrlImagen());
        Picasso.get().load(peliculas.get(i).getUrlimagen()).into(holder.imageView);
       final int i2 =i;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.getManagerPelicula().setPeliculaAcutal(peliculas.get(i2).getId());
                try {
                    mainActivity.CargarPeliculaAvista();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return peliculas.size();
    }
}
