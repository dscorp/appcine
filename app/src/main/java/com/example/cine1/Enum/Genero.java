package com.example.cine1.Enum;

public enum Genero {
    ACCION("Accion"),
    COMEDIA("Comedia"),
    AVENTURA("Aventura"),
    FAMILIAR("Familiar"),
    CIENCIAFICCION("Ciencia Ficcion"),
    SUPERHEROES("Super Heroes"),
    DRAMA("Drama"),
    ROMANCE("Romance"),
    TERROR("Terror"),
    TRILLER("Triller"),
    DEPORTE("Deporte"),
    FANTASTICO("Fantastico");



    private final String value;

    // NOTE: Enum constructor must have private or package scope. You can not use the public access
    // modifier.
    private Genero(String value) {
        this.value = value;
    }

    @Override
    public String toString(){
        return value;
    }

    public String getValue() {
        return value;
    }
}
