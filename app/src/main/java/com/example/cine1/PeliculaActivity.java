package com.example.cine1;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cine1.Model.Pelicula;
import com.squareup.picasso.Picasso;


public class PeliculaActivity extends AppCompatActivity {
    private Button btn_registrar;
    private Pelicula objpelicula;
    private TextView txt_titulo_detalle,txt_descripcion_detalle,txt_pelicula_hora_detalle;
    private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pelicula);

        objpelicula = (Pelicula) getIntent().getSerializableExtra("pelicula");

        txt_titulo_detalle = findViewById(R.id.txt_titulo_detalle);
        txt_descripcion_detalle = findViewById(R.id.txt_descripcion_detalle);
        txt_pelicula_hora_detalle = findViewById(R.id.txt_pelicula_hora_detalle);
        imageView = findViewById(R.id.ImviePdetalle);

        Picasso.get().load(objpelicula.getUrlimagenLarga()).into(imageView);
        txt_pelicula_hora_detalle.setText(Integer.toString(objpelicula.getDuracion()) + " min");
        txt_titulo_detalle.setText(objpelicula.getTitulo());
        txt_descripcion_detalle.setText(objpelicula.getDescripcion());
    }
}
