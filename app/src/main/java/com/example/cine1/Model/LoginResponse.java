package com.example.cine1.Model;

import java.io.Serializable;

public class LoginResponse implements Serializable {

    private boolean error;
    private String mensaje;
    private User user;
    private  String token;


    public LoginResponse() {
    }


    public LoginResponse(boolean error, String mensaje, User user, String token) {
        this.error = error;
        this.mensaje = mensaje;
        this.user = user;
        this.token = token;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "error=" + error +
                ", mensaje='" + mensaje + '\'' +
                ", user=" + user +
                ", token='" + token + '\'' +
                '}';
    }
}
