package com.example.cine1.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Pais implements Parcelable {

    private int idPais;
    private String nombre;

    public Pais(){

    }

    public Pais(Parcel registro){
        idPais = registro.readInt();
        nombre = registro.readString();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel registro, int flags) {
        registro.writeInt(idPais);
        registro.writeString(nombre);
    }

    public static final Parcelable.Creator<Pais> CREATOR = new Parcelable.Creator<Pais>() {
        @Override
        public Pais createFromParcel(Parcel Registro) {
            return new Pais(Registro);
        }

        @Override
        public Pais[] newArray(int Size) {
            return new Pais[Size];
        }
    };
}
