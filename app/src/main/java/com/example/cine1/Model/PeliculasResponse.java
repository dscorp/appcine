package com.example.cine1.Model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PeliculasResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("messaje")
    @Expose
    private String messaje;
    @SerializedName("peliculas")
    @Expose
    private List<Pelicula> peliculas = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessaje() {
        return messaje;
    }

    public void setMessaje(String messaje) {
        this.messaje = messaje;
    }

    public List<Pelicula> getPeliculas() {
        return peliculas;
    }

    public void setPeliculas(List<Pelicula> peliculas) {
        this.peliculas = peliculas;
    }

}