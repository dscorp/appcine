package com.example.cine1.Model;

import java.io.Serializable;
import java.util.List;

import com.example.cine1.Util.Util;


public class Pelicula  implements Serializable {


    private Integer id;

    private String titulo;

    private String fechaEstreno;

    private Integer esestreno;

    private String urlimagen;

    private String urlimagenLarga;

    private String descripcion;

    private Integer duracion;

    private Object createdAt;

    private Object updatedAt;

    private Object puntos;

    private String pais;

    private List<Genero> generos = null;

    private List<Actore> actores = null;

    private List<Funcione> funciones = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFechaEstreno() {
        return fechaEstreno;
    }

    public void setFechaEstreno(String fechaEstreno) {
        this.fechaEstreno = fechaEstreno;
    }

    public Integer getEsestreno() {
        return esestreno;
    }

    public void setEsestreno(Integer esestreno) {
        this.esestreno = esestreno;
    }

    public String getUrlimagen() {
        return Util.URL_IMAGENES+urlimagen;
    }

    public void setUrlimagen(String urlimagen) {
        this.urlimagen = urlimagen;
    }

    public String getUrlimagenLarga() {
        return Util.URL_IMAGENES+urlimagenLarga;
    }

    public void setUrlimagenLarga(String urlimagenLarga) {
        this.urlimagenLarga = urlimagenLarga;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getPuntos() {
        return puntos;
    }

    public void setPuntos(Object puntos) {
        this.puntos = puntos;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getGeneros() {
        String cadena="";
        for (Genero genero:this.generos) {
            cadena=cadena+genero.getNombre()+", ";
        }
        return cadena;
    }

    public void setGeneros(List<Genero> generos) {
        this.generos = generos;
    }

    public String getActores() {
        String cadena="";
        for (Actore actor:this.actores) {
            cadena=cadena+actor.getNombre()+", ";
        }
        return cadena;

    }

    public void setActores(List<Actore> actores) {
        this.actores = actores;
    }

    public List<Funcione> getFunciones() {
        return funciones;
    }

    public void setFunciones(List<Funcione> funciones) {
        this.funciones = funciones;
    }
}