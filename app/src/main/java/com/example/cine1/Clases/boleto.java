package com.example.cine1.Clases;

public class boleto {


    private String idboleto;
    private int cantidad;
    private int idfuncion;
    private int idusuario;

    public String getIdboleto() {
        return idboleto;
    }

    public void setIdboleto(String idboleto) {
        this.idboleto = idboleto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getIdfuncion() {
        return idfuncion;
    }

    public void setIdfuncion(int idfuncion) {
        this.idfuncion = idfuncion;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }
}
