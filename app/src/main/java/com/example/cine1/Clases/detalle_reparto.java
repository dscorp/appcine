package com.example.cine1.Clases;

public class detalle_reparto {

    private String idreparto;
    private String idactor;
    private String idpelicula;

    public String getIdreparto() {
        return idreparto;
    }

    public void setIdreparto(String idreparto) {
        this.idreparto = idreparto;
    }

    public String getIdactor() {
        return idactor;
    }

    public void setIdactor(String idactor) {
        this.idactor = idactor;
    }

    public String getIdpelicula() {
        return idpelicula;
    }

    public void setIdpelicula(String idpelicula) {
        this.idpelicula = idpelicula;
    }
}
