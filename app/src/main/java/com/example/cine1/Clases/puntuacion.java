package com.example.cine1.Clases;

public class puntuacion {

    private int idpuntuacion;
    private int idusuario;
    private String idpelicula;

    public int getIdpuntuacion() {
        return idpuntuacion;
    }

    public void setIdpuntuacion(int idpuntuacion) {
        this.idpuntuacion = idpuntuacion;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getIdpelicula() {
        return idpelicula;
    }

    public void setIdpelicula(String idpelicula) {
        this.idpelicula = idpelicula;
    }
}
