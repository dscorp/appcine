package com.example.cine1.Clases;

public class actor {
    private String idactor;
    private String nombre;
    private String pais;


    public String getIdactor() {
        return idactor;
    }

    public void setIdactor(String idactor) {
        this.idactor = idactor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }
}
