package com.example.cine1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cine1.Model.Pelicula;

public class VentaActivity extends AppCompatActivity {
    private Button btn_pagar_venta;
    private Pelicula objpelicula;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venta);

        objpelicula = (Pelicula)getIntent().getSerializableExtra("pelicula");

        ImageView img_pelicula_venta = (ImageView)findViewById(R.id.img_pelicula_venta);
        TextView txt_titulo_venta = (TextView)findViewById(R.id.txt_titulo_venta);

        txt_titulo_venta.setText(objpelicula.getTitulo());
//        img_pelicula_venta.setImageResource(objpelicula.getUrlImagen());

        btn_pagar_venta = findViewById(R.id.btn_pagar_venta);

        btn_pagar_venta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Boton de Compra",Toast.LENGTH_SHORT).show();
            }
        });

    }
}
