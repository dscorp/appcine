package com.example.cine1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;
import com.example.cine1.Api.APIService;
import com.example.cine1.Api.ApiUtils;
import com.example.cine1.Control.ManagerPelicula;
import com.example.cine1.Model.Pelicula;
import com.example.cine1.Model.PeliculasResponse;
import com.example.cine1.Model.User;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        ProfileFragment.OnFragmentInteractionListener, CarteleraFragment.OnFragmentInteractionListener, View.OnClickListener {

    private RecyclerView rvsPeliculasSplash;
    private Intent nextActivity;
    private ManagerPelicula managerPelicula;
    private RecyclerView.Adapter rvadapter;
    private Pelicula objpelicula;
    private TextView txt_nombrepelicula, txt_fechaestreno, txt_pais_pelicula, txt_generospelicula_, txt_reparto, txt_pelicula_hora_main;
    private ImageView image_large;
    private User objusuarioactual;
    private Fragment fragmento = null;
    private String key_name = "login";
    private SharedPreferences preferences;
    private APIService mAPIService;

    public static Pelicula pelicula = new Pelicula();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.objusuarioactual = (User) getIntent().getParcelableExtra("usuario");
//        preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
//        token = preferences.getString("token", "");
        mAPIService = ApiUtils.getAPIService();
        Toolbar toolbar = findViewById(R.id.toolbar);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.spinnerCate, android.R.layout.simple_spinner_item);//        setSupportActionBar(toolbar);
        txt_nombrepelicula = (TextView) findViewById(R.id.txt_titulopelicula);
        txt_nombrepelicula.setMovementMethod(new ScrollingMovementMethod());
        txt_fechaestreno = (TextView) findViewById(R.id.txt_fechaestreno_pelicula);
        txt_pais_pelicula = (TextView) findViewById(R.id.txt_pais_pelicula);
        txt_generospelicula_ = (TextView) findViewById(R.id.txt_generospelicula_);
        txt_reparto = (TextView) findViewById(R.id.txt_reparto);
        txt_pelicula_hora_main = (TextView) findViewById(R.id.txt_pelicula_hora_main);
        image_large = (ImageView) findViewById(R.id.image_large);

//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        //Botones
        Button btn_comprar = (Button) findViewById(R.id.btn_comprar);
        btn_comprar.setOnClickListener(this);
        Button btn_masinformacion = (Button) findViewById(R.id.btn_masinformacion);
        btn_masinformacion.setOnClickListener(this);
//RecycerView
        rvsPeliculasSplash = (RecyclerView) findViewById(R.id.rvPeliculasSplash);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvsPeliculasSplash.setHasFixedSize(true);
        rvsPeliculasSplash.setItemAnimator(new DefaultItemAnimator());
        rvsPeliculasSplash.setLayoutManager(layoutManager);
        //inicia la peticion de las peliculas hacia la api
        sendPost();
    }


    //accion de los botones
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_comprar://boton Comprar
                nextActivity = new Intent(MainActivity.this, VentaActivity.class);
                nextActivity.putExtra("pelicula", managerPelicula.getPeliculaActual());//pasa la pelicula seleccinada hacia la vista de compra
                startActivity(nextActivity);
                break;

            case R.id.btn_masinformacion://boton mas informacion
                nextActivity = new Intent(MainActivity.this, PeliculaActivity.class);
                nextActivity.putExtra("pelicula", managerPelicula.getPeliculaActual());//pasa la pelicula seleccionada hacia la vista de  mas informacion
                startActivity(nextActivity);
                Animatoo.animateCard(this);
                break;
        }
    }

    public void CargarPeliculaAvista() throws IOException {

        objpelicula = managerPelicula.getPeliculaActual();
        Picasso.get().load(objpelicula.getUrlimagenLarga()).fit().into(image_large);
        txt_nombrepelicula.setText(objpelicula.getTitulo());
        txt_pelicula_hora_main.setText(Integer.toString(objpelicula.getDuracion()) + " min");
        txt_fechaestreno.setText(objpelicula.getFechaEstreno().toString());
        txt_pais_pelicula.setText(objpelicula.getPais());
        txt_generospelicula_.setText(objpelicula.getGeneros());
        txt_reparto.setText(objpelicula.getActores());

        rvadapter = new PeliculaSplashAdapter(managerPelicula.getPeliculas(), this);
        rvsPeliculasSplash.setAdapter(rvadapter);
    }


    public void sendPost() {
        mAPIService.ListarPeliculasEstreno().enqueue(new Callback<PeliculasResponse>() {
            @Override
            public void onResponse(Call<PeliculasResponse> call, Response<PeliculasResponse> response) {
                if (response.isSuccessful()) {
                    managerPelicula = new ManagerPelicula((ArrayList<Pelicula>) response.body().getPeliculas());
                    try {
                        CargarPeliculaAvista();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<PeliculasResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void CargarCartelera() {
        mAPIService.ListarPeliculas().enqueue(new Callback<PeliculasResponse>() {
            @Override
            public void onResponse(Call<PeliculasResponse> call, Response<PeliculasResponse> response) {
                System.out.println("CarteleraFragment.onResponse");
                System.out.println(response.code());
                if (response.isSuccessful()) {
                    managerPelicula = new ManagerPelicula((ArrayList<Pelicula>) response.body().getPeliculas());

                }
            }

            @Override
            public void onFailure(Call<PeliculasResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        boolean frag_active = false;

        if (id == R.id.nav_home) {
            if (fragmento != null) {
                getSupportFragmentManager().beginTransaction().remove(fragmento).commit();
            }
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_estreno) {
            //ESTRENO

        } else if (id == R.id.nav_cartelera) {
          //  CARTELERA
            fragmento = new CarteleraFragment();


            CargarCartelera();

                        Bundle bundle = new Bundle();
            bundle.putSerializable("listapeliculas", managerPelicula.get_allpeliculas());
            fragmento.setArguments(bundle);

            frag_active = true;

        } else if (id == R.id.nav_profile) {
            //PROFILE
            Bundle bundle = new Bundle();
            bundle.putSerializable("usuario",objusuarioactual);
            fragmento = new ProfileFragment();
            fragmento.setArguments(bundle);
            frag_active = true;

        } else if (id == R.id.nav_logout) {
            //Cerrar Session
            Cerrar_Session();
            //EXIT
            nextActivity = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(nextActivity);
            Animatoo.animateCard(this);
        }

        if (frag_active) {

            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_main, fragmento).commit();

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void Cerrar_Session() {
        preferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key_name, false);
        editor.commit();
        nextActivity = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(nextActivity);
        Animatoo.animateSplit(this);
        //            Animatoo.animateDiagonal(this);
        //            Animatoo.animateShrink(this);
        finish();
    }

    public ManagerPelicula getManagerPelicula() {
        return managerPelicula;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//
//        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            System.out.println(" Orientacion Horizontal");
//
//           image_large.getLayoutParams().height=R.dimen.AltoImagenPrincipalLandScape;
//
//
//        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
//            System.out.println(" Orientacion Vertical");
//
//
//            image_large.getLayoutParams().height=R.dimen.AltoImagenPrincipalPortail;
//
//        }


    }


}
