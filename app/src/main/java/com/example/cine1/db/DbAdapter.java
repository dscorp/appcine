//package com.example.cine1.db;
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteException;
//import android.database.sqlite.SQLiteOpenHelper;
//import android.widget.Toast;
//
//
//public class DbAdapter {
//    //Degine la base de datos y las operaciones sobre ella
//
//    private static final int DATABASE_VERSION=1;
//    private static final String DATABASE_NAME="cinedb";
//    //tablas
//    private static final String TABLE_CINE="producto";
//    //campos de las tablas
//    private static final String KEY_ROWID="rowid";
//    private static final String KEY_ID= "idCine";
//    private static final String KEY_NOMBRE="nombre";
//    private static final String KEY_DIRECCION="direccion";
//
//    //operaciones
//    private final static String TABLE_CREATE_CINE=
//            "create table "+TABLE_CINE+ "("+ KEY_ROWID+" integer primary key autoincrement," +
//                    KEY_ID+" integer not null," +
//                    KEY_NOMBRE+" text not null," +
//                   KEY_DIRECCION+" text not null)";
//
//    //variable de control
//    private DataBaseHelper DBHelper;
//    private SQLiteDatabase db;
//    private static Context context;
//
//    public DbAdapter(Context context)
//    {
//    this.context=context;
//    }
//
//    private static class DataBaseHelper extends SQLiteOpenHelper
//    {
//        public DataBaseHelper(Context context)
//        {
//            super(context,DATABASE_NAME,null,DATABASE_VERSION);
//        }
//
//        @Override
//        public void onCreate(SQLiteDatabase db) {
//
//        db.execSQL(TABLE_CREATE_CINE);
//        }
//
//        @Override
//        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//            System.out.println(TABLE_CREATE_CINE);
//        db.execSQL("DROP TABLE IF EXISTS "+TABLE_CINE);
//        db.execSQL(TABLE_CREATE_CINE);
//        }
//    }
//
//    public DbAdapter open() throws SQLiteException
//    {
//        try {
//            DBHelper = new DataBaseHelper(context);
//        db= DBHelper.getWritableDatabase();
//        }catch (SQLiteException e)
//        {
//            Toast.makeText(context,"ERROR - Database : "+e.getMessage(),Toast.LENGTH_SHORT).show();
//            e.printStackTrace();
//        }
//
//        return this;
//    }
//
//    public boolean isOpen()
//    {
//        if(db==null) return false;
//        else return true;
//    }
//
//    public void close()
//    {
//    DBHelper.close();
//    db.close();
//    }
//
//
//    public void getCine()
//    {
//
//        String query= "select * from cine";
//        System.out.println(query);
//
//        try {
//            //cursos para recorrer toda la tabla
//            Cursor cursor = db.rawQuery(query,null);
//            if(cursor.moveToFirst())
//            {
//
//            }
//            cursor.close();
//        }
//        catch (Exception e)
//        {
//          e.printStackTrace();
//        }
//
//
//    }
//
//
//}
