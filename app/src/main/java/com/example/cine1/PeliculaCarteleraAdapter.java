package com.example.cine1;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cine1.Model.Pelicula;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PeliculaCarteleraAdapter extends RecyclerView.Adapter<PeliculaCarteleraAdapter.ViewHolder> {

private List<Pelicula> peliculas;
private Context context;

    public PeliculaCarteleraAdapter(List<Pelicula> peliculas, Context context) {
        this.peliculas = peliculas;
        this.context = context;
    }


    public class ViewHolder extends RecyclerView.ViewHolder
    {
    TextView tvtitulo,tvdescripcion;
    ImageView ivpelicula;
    Button btnverdetalles;


        public ViewHolder(@NonNull View v) {
            super(v);
            tvtitulo= (TextView)  itemView.findViewById(R.id.tvcarteleratitulo);
            tvdescripcion= (TextView) itemView.findViewById(R.id.tvdcarteleradetalle);
            ivpelicula =(ImageView) itemView.findViewById(R.id.ivcartelera);
            btnverdetalles= (Button) itemView.findViewById(R.id.btncvverdetalles);
        }


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_card_view_cartelera,viewGroup,false);
        PeliculaCarteleraAdapter.ViewHolder vh = new PeliculaCarteleraAdapter.ViewHolder(v);
        context=viewGroup.getContext();
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.tvtitulo.setText(peliculas.get(i).getTitulo());
        viewHolder.tvdescripcion.setText(peliculas.get(i).getDescripcion());
        Picasso.get().load(peliculas.get(i).getUrlimagen()).into(viewHolder.ivpelicula);
        final int e =i;
        viewHolder.btnverdetalles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,PeliculaActivity.class);
                i.putExtra("pelicula",peliculas.get(e));
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return peliculas.size();
    }







}
